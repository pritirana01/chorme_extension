/*document.getElementById('ask').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    alert(`Ask: ${text}`);
  });
  
  document.getElementById('summarize').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    alert(`Summarize: ${text}`);
  });
  
  document.getElementById('explain').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    alert(`Explain: ${text}`);
  });
  // Function to handle fetching data from the server and rendering it on UI
async function fetchDataFromServerAndRender(textInput) {
    try {
        const response = await fetch('/api/openai/chat', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                messages: [
                    { role: 'user', content: textInput }
                ],
                n: 1,
                max_tokens: 150
            })
        });
        const data = await response.json();
        const result = data.choices[0];
        const messageContent = result.message.content;
        renderMessageOnUI(messageContent);
    } catch (error) {
        console.error('Error:', error);
    }
}*/

// Function to render the message content on the UI
//function renderMessageOnUI(messageContent) {
    //const messageContainer = document.getElementById('messageContainer');
  //  messageContainer.textContent = messageContent;
//}

// Event listener for when user enters text and presses enter
/*document.getElementById('textInput').addEventListener('keypress', function(event) {
    if (event.key === 'Enter') {
        const textInput = event.target.value;
        fetchDataFromServerAndRender(textInput);
        event.target.value = ''; // Clear input field
    }
})
async function fetchDataFromServerAndRender(textInput) {
    try {
        const response = await fetch('http://localhost:3000/api/openai/chat', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                messages: [
                    { role: 'user', content: textInput }
                ],
                n: 1,
                max_tokens: 150
            })
        });
        const data = await response.json();
        const result = data.choices[0];
        const messageContent = result.message.content;

        renderMessageOnUI(messageContent);
    } catch (error) {
        console.error('Error:', error);
    }
}

function renderMessageOnUI(messageContent) {
    const messageContainer = document.getElementById('messageContainer');
    messageContainer.textContent = messageContent;
}

document.getElementById('ask').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender(`Ask: ${text}`);
});

document.getElementById('summarize').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender(`Summarize: ${text}`);
});

document.getElementById('explain').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender(`Explain: ${text}`);
});*/
document.getElementById('textInput').addEventListener('keypress', function(event) {
    if (event.key === 'Enter') {
      const textInput = event.target.value;
      fetchDataFromServerAndRender('ask', textInput);
      event.target.value = ''; // Clear input field
    }
  });
  
  async function fetchDataFromServerAndRender(action, textInput) {
    try {
      const response = await fetch('http://localhost:3000/api/openai/chat', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          messages: [
            { role: 'user', content: `${action}: ${textInput}` }
          ],
          n: 1,
          max_tokens: 150
        })
      });
      const data = await response.json();
      const result = data.choices[0];
      const messageContent = result.message.content;
  
      renderMessageOnUI(messageContent);
    } catch (error) {
      console.error('Error:', error);
    }
  }
  
  function renderMessageOnUI(messageContent) {
    const messageContainer = document.getElementById('messageContainer');
    messageContainer.textContent = messageContent;
  }
  
  document.getElementById('ask').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender('ask', text);
  });
  
  document.getElementById('summarize').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender('summarize', text);
  });
  
  document.getElementById('explain').addEventListener('click', function() {
    const text = document.getElementById('textInput').value;
    fetchDataFromServerAndRender('explain', text);
  });
  

  