/*const CACHE_NAME = 'extension-cache-v1';
const urlsToCache = [
  '/',
  '/index.html', // Ensure these paths are correct
  '/styles.css',
  '/script.js',
  '/ask',
  '/summarize',
  '/explain'
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});





chrome.runtime.onInstalled.addListener(() => {
    console.log('AI Extension Installed');
  });
  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.type === 'content') {
      fetch('http://localhost:3000/ask', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ pageContent: message.pageContent })
      })
      .then(response => response.json())
      .then(data => sendResponse(data))
      .catch(error => sendResponse({ error: 'Failed to push content.' }));
      return true; // Keep the message channel open for sendResponse
    }
})

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.type === 'content') {
      fetch('http://localhost:3000/summarize', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ pageContent: message.pageContent })
      })
      .then(response => response.json())
      .then(data => sendResponse(data))
      .catch(error => sendResponse({ error: 'Failed to push content.' }));
      return true; // Keep the message channel open for sendResponse
    }
})
chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.type === 'content') {
      fetch('http://localhost:3000/explain', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ pageContent: message.pageContent })
      })
      .then(response => response.json())
      .then(data => sendResponse(data))
      .catch(error => sendResponse({ error: 'Failed to push content.' }));
      return true; // Keep the message channel open for sendResponse
    }
})*/
const CACHE_NAME = 'extension-cache-v1';
const urlsToCache = [
  '/',
  '/index.html',
  '/styles.css',
  '/script.js',
  '/ask',
  '/summarize',
  '/explain'
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request)
      .then(response => {
        if (response) {
          return response; // Return the cached version
        }
        return fetch(event.request).then(
          response => {
            if (!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }
            let responseToCache = response.clone();
            caches.open(CACHE_NAME)
              .then(cache => {
                cache.put(event.request, responseToCache);
              });
            return response;
          }
        );
      })
  );
});

chrome.runtime.onInstalled.addListener(() => {
  console.log('AI Extension Installed');
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.type === 'content') {
    let endpoint = '';
    if (message.action === 'ask') {
      endpoint = 'ask';
    } else if (message.action === 'summarize') {
      endpoint = 'summarize';
    } else if (message.action === 'explain') {
      endpoint = 'explain';
    }

    if (endpoint) {
      fetch(`http://localhost:3000/${endpoint}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ pageContent: message.pageContent })
      })
      .then(response => response.json())
      .then(data => sendResponse(data))
      .catch(error => sendResponse({ error: 'Failed to push content.' }));
      return true; // Keep the message channel open for sendResponse
    }
  }
});
