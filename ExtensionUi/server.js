// server.js
/*const express = require('express');
const axios = require('axios');

const app = express();
const PORT =  3000;
const OPENAI_API_KEY = '';

app.get('/api/openai', async (req, res) => {
  try {
    const response = await axios.post('https://api.openai.com/v1/models', {
      
    }, {
      headers: {
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
        'Content-Type': 'application/json',
      },
    });
    res.json(response.data);
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});*/
// server.js
/*const express = require('express');
const axios = require('axios');
const cors=require('cors')

const app = express();
const PORT = process.env.PORT || 3000;
const OPENAI_API_KEY = 'sk-proj-wCGfYiF2lYWuDD7VMGj0T3BlbkFJbLacmIODHiMh89u9dltE'; // Replace this with your actual OpenAI API key
app.use(cors())
app.use(express.json());
app.post('/ask', async (req, res) => {
  const { messages, n, max_tokens } = req.body;

  try {
    const response = await axios.post('https://api.openai.com/v1/chat/completions', {
      model: 'gpt-4',
      messages: messages,
      n: n,
      max_tokens: max_tokens
    }, {
      headers: {
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
        'Content-Type': 'application/json'
      }
    });

    res.json(response.data);
    //console.log(response.data[0])
    const result=response.data.choices[0]
    const msg=result.message
    console.log(msg.content)
   /* result.forEach(element => {
     //const choice=element.choices
     // console.log(choice)
     //console.log(element.choices)


    });
  
  } catch (error) {
    console.error('Error:', error.response ? error.response.data : error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});
// New summarize endpoint
app.post('/summarize', async (req, res) => {
    const { pageContent} = req.body;
  
    try {
      const response = await axios.post('https://api.openai.com/v1/chat/completions', {
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant that summarizes text.' },
          { role: 'user', content: `Summarize the following text:\n\n${pageContent}` }
        ],
       // max_tokens: 150
      }, {
        headers: {
          'Authorization': `Bearer ${OPENAI_API_KEY}`,
          'Content-Type': 'application/json'
        }
      });
  
      const summary = response.data.choices[0].message.content.trim();
      res.json({ summary });
      console.log(summary)
    } catch (error) {
      console.error('Error:', error.response ? error.response.data : error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
  
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});*/

/*const express = require('express');
const bodyParser = require('body-parser');
//const { Pool } = require('pg');
const app = express();
const PORT = 3000;
const cors = require('cors');
app.use(cors());
const OPENAI_API_KEY = 'sk-proj-wCGfYiF2lYWuDD7VMGj0T3BlbkFJbLacmIODHiMh89u9dltE';
/*const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'DemoDb',
    password: '1234',
    port: 5432,
});
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js')
      .then(registration => {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
      })
      .catch(error => {
        console.log('ServiceWorker registration failed: ', error);
      });
  });
}





app.use(bodyParser.json());
/*app.post('/push', async (req, res) => {
    const { pageContent } = req.body;
    try {
        await pool.query('INSERT INTO contents (content) VALUES ($1)', [pageContent]);
        res.json({ response: 'Content pushed successfully.' });
    } catch (error) {
        res.status(500).json({ error: 'Failed to push content.' });
    }
});
app.post('/summarize', async (req, res) => {
    const { pageContent } = req.body;
    const prompt = `Summarize the following content: ${pageContent}`;
    try {
        const fetch = (await import('node-fetch')).default;
        const response = await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${OPENAI_API_KEY}`
            },
            body: JSON.stringify({
                model: 'gpt-4',
                messages: [
                    { role: 'system', content: 'You are a helpful assistant.' },
                    { role: 'user', content: prompt }
                ]
            })
        });
        const data = await response.json();
        res.json({ response: data.choices[0].message.content });
    } catch (error) {
        res.status(500).json({ error: 'Failed to fetch AI response.' });
    }
});
app.post('/explain', async (req, res) => {
    const { userInput, pageContent } = req.body;
    const prompt = `Explain this in simple terms: "${userInput}" from the context of: ${pageContent}`;
    try {
        const fetch = (await import('node-fetch')).default;
        const response = await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${OPENAI_API_KEY}`
            },
            body: JSON.stringify({
                model: 'gpt-4',
                messages: [
                    { role: 'system', content: 'You are a helpful assistant.' },
                    { role: 'user', content: prompt }
                ]
            })
        });
        const data = await response.json();
        res.json({ response: data.choices[0].message.content });
    } catch (error) {
        res.status(500).json({ error: 'Failed to fetch AI response.' });
    }
});
app.post('/ask', async (req, res) => {
    const { userInput, pageContent } = req.body;
    const prompt = `Based on this content: ${pageContent}, answer this question: ${userInput}`;
    try {
        const fetch = (await import('node-fetch')).default;
        const response = await fetch('https://api.openai.com/v1/chat/completions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${OPENAI_API_KEY}`
            },
            body: JSON.stringify({
                model: 'gpt-4',
                messages: [
                    { role: 'system', content: 'You are a helpful assistant.' },
                    { role: 'user', content: prompt }
                ]
            })
        });
        const data = await response.json();
        res.json({ response: data.choices[0].message.content });
    } catch (error) {
        res.status(500).json({ error: 'Failed to fetch AI response.' });
    }
    
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});*/
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fetch = require('node-fetch'); // Ensure node-fetch is installed

const app = express();
const PORT = 3000;
const OPENAI_API_KEY = 'sk-proj-wCGfYiF2lYWuDD7VMGj0T3BlbkFJbLacmIODHiMh89u9dltE';

app.use(cors());
app.use(bodyParser.json());

app.post('/summarize', async (req, res) => {
  const { pageContent } = req.body;
  const prompt = `Summarize the following content: ${pageContent}`;
  try {
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt }
        ]
      })
    });
    const data = await response.json();
    res.json({ response: data.choices[0].message.content });
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch AI response.' });
  }
});

app.post('/explain', async (req, res) => {
  const { userInput, pageContent } = req.body;
  const prompt = `Explain this in simple terms: "${userInput}" from the context of: ${pageContent}`;
  try {
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt }
        ]
      })
    });
    const data = await response.json();
    res.json({ response: data.choices[0].message.content });
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch AI response.' });
  }
});

app.post('/ask', async (req, res) => {
  const { userInput, pageContent } = req.body;
  const prompt = `Based on this content: ${pageContent}, answer this question: ${userInput}`;
  try {
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt }
        ]
      })
    });
    const data = await response.json();
    res.json({ response: data.choices[0].message.content });
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch AI response.' });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

