/**
 * v0 by Vercel.
 * @see https://v0.dev/t/ViklKBsWoyw
 * Documentation: https://v0.dev/docs#integrating-generated-code-into-your-nextjs-app
 */
import Link from "next/link"

export default function Component() {
  return (
    <div className="flex flex-col items-center justify-center h-screen bg-gradient-to-br from-indigo-500 via-purple-500 to-pink-500">
      <div className="grid grid-cols-3 gap-8">
        <Link
          className="flex flex-col items-center justify-center bg-white rounded-lg shadow-lg p-8 hover:scale-105 transition-transform"
          href="#"
        >
          <MessageCircleIcon className="w-12 h-12 text-indigo-500 mb-4" />
          <h3 className="text-xl font-bold mb-2">Ask</h3>
          <p className="text-gray-500 text-center">Open a window to ask a question.</p>
        </Link>
        <Link
          className="flex flex-col items-center justify-center bg-white rounded-lg shadow-lg p-8 hover:scale-105 transition-transform"
          href="#"
        >
          <FileTextIcon className="w-12 h-12 text-purple-500 mb-4" />
          <h3 className="text-xl font-bold mb-2">Summarize</h3>
          <p className="text-gray-500 text-center">Open a window to summarize text.</p>
        </Link>
        <Link
          className="flex flex-col items-center justify-center bg-white rounded-lg shadow-lg p-8 hover:scale-105 transition-transform"
          href="#"
        >
          <LightbulbIcon className="w-12 h-12 text-pink-500 mb-4" />
          <h3 className="text-xl font-bold mb-2">Explain</h3>
          <p className="text-gray-500 text-center">Open a window to explain a concept.</p>
        </Link>
      </div>
    </div>
  )
}

function FileTextIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M15 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7Z" />
      <path d="M14 2v4a2 2 0 0 0 2 2h4" />
      <path d="M10 9H8" />
      <path d="M16 13H8" />
      <path d="M16 17H8" />
    </svg>
  )
}


function LightbulbIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M15 14c.2-1 .7-1.7 1.5-2.5 1-.9 1.5-2.2 1.5-3.5A6 6 0 0 0 6 8c0 1 .2 2.2 1.5 3.5.7.7 1.3 1.5 1.5 2.5" />
      <path d="M9 18h6" />
      <path d="M10 22h4" />
    </svg>
  )
}


function MessageCircleIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M7.9 20A9 9 0 1 0 4 16.1L2 22Z" />
    </svg>
  )
}